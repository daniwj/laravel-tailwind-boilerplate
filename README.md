<img width="100" alt="React-icon" src="https://upload.wikimedia.org/wikipedia/commons/thumb/a/a7/React-icon.svg/512px-React-icon.svg.png"> <img width="250" alt="Tailwind-icon" src="https://refactoringui.nyc3.cdn.digitaloceanspaces.com/tailwind-logo.svg">

## ABOUT

Laravel with TailwindCSS boilerplate.

Bundled with Laravel 7.0 to utilize artisan and Laravel Mix webpack wrapper.

Other tools/assets:

-   PostCSS w/Autoprefixer
-   PurgeCSS
-   Browsersync

## INSTALL

Clone repository into empty folder

From project root, run:

```bash
composer install
php artisan serve
```

Open another terminal and run

```bash
npm install && npm run dev
```

or

```bash
yarn install && yarn dev
```

If you want webpack and browsersync to watch for changes, run `yarn watch` instead of `yarn dev`

## DOCUMENTATION

Tailwind: For full documentation, visit [tailwindcss.com](https://tailwindcss.com/).

React: For documentation for Reactjs, visit [reactjs.org](https://reactjs.org/docs/getting-started.html)

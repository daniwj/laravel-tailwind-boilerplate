<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Boilerplate</title>
    @preload
    <link rel="dns-prefetch" href="https://fonts.gstatic.com">
    <link rel="dns-prefetch" href="https://fonts.googleapis.com">
    <link rel="stylesheet" href="{{mix('/css/style.css')}}">
</head>

<body class="min-h-screen">
    <div class="app flex flex-col" id="app">

        <script src="{{mix('/js/common.js')}}" defer></script>
    </div>
</body>

</html>

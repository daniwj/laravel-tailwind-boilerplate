const mix = require("laravel-mix");
const tailwindcss = require("tailwindcss");
const resolve = require("path").resolve;
require("laravel-mix-purgecss");
require("laravel-mix-imagemin");

// TODO: Inline critical css
// TODO: Add bem-linter for postcss, and create rules for styling scss
// TODO: Add postcss support for combining duplicated selectors (on production) - (Ex.)
// TODO: Add postcss support for optimizing media queries (on production) - (Ex. css-mqpacker)

mix.js("resources/js/common.js", "public/js")
    .sass("resources/sass/style.scss", "public/css")
    .options({
        processCssUrls: false,
        postCss: [tailwindcss("./tailwind.config.js")]
    })
    .purgeCss({
        extend: {
            content: [
                "./resources/views/**/*.blade.php",
                "./resources/views/**/*.html",
                "./resources/js/**/*.{js,jsx,ts,tsx}"
            ]
        }
    })
    .imagemin(
        "images/**/*.*",
        {
            context: "resources"
        },
        {
            optipng: {
                optimizationLevel: 5
            },
            jpegtran: null,
            plugins: [
                require("imagemin-mozjpeg")({
                    quality: 70,
                    progressive: true
                }),
                { cacheFolder: resolve("./storage/app/public/images/cache") }
            ]
        }
    )
    .copyDirectory("resources/fonts", "public/fonts")
    .disableSuccessNotifications()
    .browserSync("127.0.0.1:8000");

if (mix.inProduction()) {
    mix.version();
} else {
    // Enable when debugging deep.
    // mix.sourceMaps(productionSourceMaps, "source-map");
}
